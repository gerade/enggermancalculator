package gateprotecttask.server;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Contains simple expression evaluation tests for class
 * <code>ExpressionParser</code>.
 * 
 * @author Igor Poletaev
 * 
 */
public class ExpressionParserTest {

    public static String EXPR1_1 = "(((100 - 5 )*(-1-7))/(2-11))";
    public static String EXPR1_2 = "((((2 - 1)*(7 - 5)) - 5) * 7)";
    public static String EXPR1_3 = "2 - 5";
    public static String EXPR1_4 = "(1+((2+3)*(4*5)))";

    public static String EXPR2_1 = "(((100 subtract 5 )*(-1-7))/(2-11))";
    public static String EXPR2_2 = "(((5 add 1) subtract 3 )divide by 3) multiply with 7";
    public static String EXPR2_3 = "(-9.999.999 + 9.999.999)";
    public static String EXPR2_4 = "((5 multiply with 10) - (40 divide by 4))";

    public static String EXPR3_1 = "(((hundred and one subtract six )*(minus one minus seven))/(two minus eleven))";
    public static String EXPR3_2 = "(((five add one) subtract three )divide by three) multiply with seven";
    public static String EXPR3_3 = "(-9.999.999 + 9.999.999)";
    public static String EXPR3_4 = "((five multiply with ten) - (forty divide by four))";

    public static String EXPR4_1 = "neun Millionen neunhundertneunundneunzigtausendneunhundertneunundneunzig minus 1";
    public static String EXPR4_2 = "(((5 plus 1) minus 3 ) geteilt durch 3) mal 7";
    public static String EXPR4_3 = "(Minus Millionen neunhundertneunundneunzigtausendneunhundertneunundneunzig + 1.999.999)";
    public static String EXPR4_4 = "((5 mal 10) minus (40 geteilt durch 4))";

    @Test
    public void testSimpleOperation() {
        assertEquals(new Integer(84), ExpressionParser.evaluateExpression(EXPR1_1));
        assertEquals(new Integer(-21), ExpressionParser.evaluateExpression(EXPR1_2));
        assertEquals(new Integer(-3), ExpressionParser.evaluateExpression(EXPR1_3));
        assertEquals(new Integer(101), ExpressionParser.evaluateExpression(EXPR1_4));
    }

    @Test
    public void testNumeralOperations() {
        assertEquals(new Integer(84), ExpressionParser.evaluateExpression(EXPR2_1));
        assertEquals(new Integer(7), ExpressionParser.evaluateExpression(EXPR2_2));
        assertEquals(new Integer(0), ExpressionParser.evaluateExpression(EXPR2_3));
        assertEquals(new Integer(40), ExpressionParser.evaluateExpression(EXPR2_4));
    }

    @Test
    public void testEnglishNumerals() {
        assertEquals(new Integer(84), ExpressionParser.evaluateExpression(EXPR3_1));
        assertEquals(new Integer(7), ExpressionParser.evaluateExpression(EXPR3_2));
        assertEquals(new Integer(0), ExpressionParser.evaluateExpression(EXPR3_3));
        assertEquals(new Integer(40), ExpressionParser.evaluateExpression(EXPR3_4));
    }

    @Test
    public void testGermanNumerals() {
        assertEquals(new Integer(9999998), ExpressionParser.evaluateExpression(EXPR4_1));
        assertEquals(new Integer(7), ExpressionParser.evaluateExpression(EXPR4_2));
        assertEquals(new Integer(0), ExpressionParser.evaluateExpression(EXPR4_3));
        assertEquals(new Integer(40), ExpressionParser.evaluateExpression(EXPR4_4));
    }
}
