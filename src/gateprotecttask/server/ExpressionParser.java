package gateprotecttask.server;

import gateprotecttask.server.model.Addition;
import gateprotecttask.server.model.Brackets;
import gateprotecttask.server.model.Division;
import gateprotecttask.server.model.Multiplication;
import gateprotecttask.server.model.Subtraction;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import org.apache.log4j.Logger;

public class ExpressionParser {

    private static final String OPERATORS_PATTERN;
    private static final String SIGN_OR_OPEN_BRACKET_PATTERN = "[\\(\\=\\-\\/\\*]{1}";

    private static Logger log = Logger.getLogger(ExpressionParser.class);

    /**
     * Generate operators and brackets pattern based on operations enumerations.
     * Also build English and German numerals patterns to detect numeral
     * language.
     */
    static {

        StringBuilder operatorsList = new StringBuilder();
        operatorsList.append("(");
        for (Brackets element : Brackets.values()) {
            operatorsList.append(wrapWithRegexpOr(element.toString()));
        }
        for (Addition element : Addition.values()) {
            operatorsList.append(wrapWithRegexpOr(element.toString()));
        }
        for (Subtraction element : Subtraction.values()) {
            operatorsList.append(wrapWithRegexpOr(element.toString()));
        }
        for (Multiplication element : Multiplication.values()) {
            operatorsList.append(wrapWithRegexpOr(element.toString()));
        }
        for (Division element : Division.values()) {
            operatorsList.append(wrapWithRegexpOr(element.toString()));
        }
        operatorsList.setCharAt(operatorsList.length() - 1, ')');
        OPERATORS_PATTERN = operatorsList.toString();
    }

    /**
     * Prepares expression for Dijkstra's Two-Stack Algorithm (puts every
     * operands pair into brackets)
     * <p>
     * Attaches minus sign to the operands in case if previous element was
     * arithmetic operator or open bracket.
     * <p>
     * Removes input artifacts.
     * 
     * @param expression
     *            expression to prepare
     * @return
     * @throws Exception
     */
    private static List<String> prepareExpression(String expression) throws NumberFormatException {
        LinkedList<String> result;
        Splitter splitter = new Splitter(OPERATORS_PATTERN, true);
        LinkedList<String> tokenizedExpression = splitter.split(expression);

        // remove useless elements
        removeEmptyAndWhitespaceElements(tokenizedExpression);

        // convert words to numbers
        // TODO
        result = parseWords(tokenizedExpression);

        // merge elements that represent signed number (only applied to + and -)
        result = mergePreSign(result);

        // TODO prepare expression for Dijkstra's Two-Stack Algorithm
        addBrackets(result);

        return result;
    }

    private static LinkedList<String> parseWords(LinkedList<String> tokenizedExpression)
            throws NumberFormatException {
        String[] result = (String[]) tokenizedExpression.toArray(new String[tokenizedExpression
                .size()]);
        String currentElement;
        for (int i = 0; i < result.length; i++) {
            currentElement = result[i].trim();
            if (Addition.containsIgnoreCase(currentElement)) {
                result[i] = Addition.PLUS_SYMBOL.toString();
            } else if (Subtraction.containsIgnoreCase(currentElement)) {
                result[i] = Subtraction.MINUS_SYMBOL.toString();
            } else if (Multiplication.containsIgnoreCase(currentElement)) {
                result[i] = Multiplication.MULTIPLY_SYMBOL.toString();
            } else if (Division.containsIgnoreCase(currentElement)) {
                result[i] = Division.DIVISION_SYMBOL.toString();
            } else if (currentElement.contains(".")) {
                // TODO just remove sign '.' for now. Format support shoud be
                // added
                result[i] = currentElement.replaceAll("[\\.]", "");
            } else if (!currentElement.matches("[\\(\\)\\d]+")) {
                log.debug(currentElement);
                // TODO: here language should be detected.
                // currently parser tries to parse numerals

                Long value = ConvertEnglishWordToNumber.parse(currentElement);
                if (value == null) {
                    // try German language
                    value = ConvertGermanWordToNumber.parse(currentElement);
                }
                if (value == null) {
                    throw new NumberFormatException("Cannot parse numeral");
                } else {
                    result[i] = String.valueOf(value);
                }
            }
        }
        return new LinkedList<String>(Arrays.asList(result));
    }

    /**
     * Adds brackets to input expression
     * 
     * @param splittedExpression
     *            input expression
     */
    private static void addBrackets(LinkedList<String> splittedExpression) {
        if (!Brackets.OPEN.toString().equals(splittedExpression.getFirst())
                || !Brackets.CLOSE.toString().equals(splittedExpression.getLast())) {
            splittedExpression.addFirst(Brackets.OPEN.toString());
            splittedExpression.addLast(Brackets.CLOSE.toString());
        }
    }

    /**
     * merge elements that represent signed number (only applied to + and -)
     * 
     * @param tokenizedExpression
     * @return
     */
    private static LinkedList<String> mergePreSign(LinkedList<String> tokenizedExpression) {
        LinkedList<String> result = new LinkedList<String>();
        String prev = "";
        String curr;
        String next;
        int i = 1;
        while (i < tokenizedExpression.size() - 1) {
            prev = tokenizedExpression.get(i - 1).trim();
            curr = tokenizedExpression.get(i).trim();
            next = tokenizedExpression.get(i + 1).trim();
            if ((Subtraction.MINUS_SYMBOL.toString().equals(curr) || Addition.PLUS_SYMBOL
                    .toString().equals(curr)) && prev.matches(SIGN_OR_OPEN_BRACKET_PATTERN)) {
                result.add(prev);
                result.add(curr + next);
                i += 3;

            } else {
                result.add(prev);
                result.add(curr);
                if (i + 2 == tokenizedExpression.size() - 1) {
                    result.add(next);
                }
                i += 2;
            }
        }
        if (i == tokenizedExpression.size()) {
            result.add(tokenizedExpression.getLast());
        }
        return result;
    }

    /**
     * remove empty elements
     * 
     * @param tokenizedExpression
     */
    private static void removeEmptyAndWhitespaceElements(LinkedList<String> tokenizedExpression) {
        Iterator<String> iterator = tokenizedExpression.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().trim().isEmpty()) {
                iterator.remove();
            }
        }
    }

    /**
     * Dijkstra's Two-Stack Algorithm for Expression Evaluation is used.
     * 
     * @param expression
     *            to evaluate
     * @return
     */
    public static Integer evaluateExpression(String expression) {
        List<String> tokenizedExpression;
        try {
            tokenizedExpression = prepareExpression(expression);
        } catch (NumberFormatException e) {
            log.error(e + expression);
            return null;
        }

        Stack<String> operations = new Stack<String>();
        Stack<String> values = new Stack<String>();

        for (String currentElement : tokenizedExpression) {
            currentElement = currentElement.trim();
            if (currentElement.isEmpty()) {
                continue;
            }
            if (Brackets.OPEN.toString().equals(currentElement)) {
                // nothing to do
            } else if (Addition.PLUS_SYMBOL.toString().equals(currentElement)) {
                operations.push(Addition.PLUS_SYMBOL.toString());
            } else if (Subtraction.MINUS_SYMBOL.toString().equals(currentElement)) {
                operations.push(Subtraction.MINUS_SYMBOL.toString());
            } else if (Multiplication.MULTIPLY_SYMBOL.toString().equals(currentElement)) {
                operations.push(Multiplication.MULTIPLY_SYMBOL.toString());
            } else if (Division.DIVISION_SYMBOL.toString().equals(currentElement)) {
                operations.push(Division.DIVISION_SYMBOL.toString());
            } else if (Brackets.CLOSE.toString().equals(currentElement)) {
                String op = operations.pop();
                String v = values.pop();
                if (Addition.PLUS_SYMBOL.toString().equals(op)) {
                    v = String.valueOf(Integer.parseInt(values.pop()) + Integer.parseInt(v));
                } else if (Subtraction.MINUS_SYMBOL.toString().equals(op)) {
                    v = String.valueOf(Integer.parseInt(values.pop()) - Integer.parseInt(v));
                } else if (Multiplication.MULTIPLY_SYMBOL.toString().equals(op)) {
                    v = String.valueOf(Integer.parseInt(values.pop()) * Integer.parseInt(v));
                } else if (Division.DIVISION_SYMBOL.toString().equals(op)) {
                    v = String.valueOf(Integer.parseInt(values.pop()) / Integer.parseInt(v));
                }
                values.push(v);
            } else {
                values.push(currentElement);
            }
        }

        return Integer.parseInt(values.pop());
    }

    private static String wrapWithRegexpOr(String element) {
        String result;
        if (!element.matches("[a-zA-Z ]*")) {
            result = "\\" + element;
        } else {
            result = element;
        }
        result = result + "|";
        return result;
    }
}
