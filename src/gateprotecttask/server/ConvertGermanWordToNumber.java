package gateprotecttask.server;

import org.apache.log4j.Logger;

import gateprotecttask.server.model.GermanNumerals;
import gateprotecttask.server.model.GermanPowerNumerals;

/**
 * Converts German word numerals to long. German language uses "ones" before the
 * "tens" rule.
 * 
 * @author Igor Poletaev
 * 
 */
public class ConvertGermanWordToNumber {
    private static Logger log = Logger.getLogger(ConvertGermanWordToNumber.class);
    private static final String UND_PATTERN = "(und)";
    private static final String UND_WORD = "und";

    public static Long parse(String text) {

        // "-" is useless and can be removed
        text = text.toLowerCase();
        long sum = 0;
        boolean processed = false;
        for (int i = 0; i < GermanPowerNumerals.values().length; i++) {
            text = text.replaceAll(GermanPowerNumerals.values()[i].getNumeral(), " "
                    + GermanPowerNumerals.values()[i].getNumeral() + " ");
            int index = text.indexOf(GermanPowerNumerals.values()[i].getNumeral());
            if (index >= 0) {

                String before = text.substring(0, index).trim();

                String after = text.substring(
                        index + GermanPowerNumerals.values()[i].getNumeral().length()).trim();

                if (before.isEmpty()) {
                    before = GermanNumerals.ONE.getNumeral();
                }
                if (after.isEmpty()) {
                    after = GermanNumerals.ZERO.getNumeral();
                }
                Long beforeValue = parseNumerals(before);
                Long afterValue = parse(after);
                if (beforeValue == null || afterValue == null) {
                    return null;
                }
                sum = beforeValue * GermanPowerNumerals.values()[i].getValue() + afterValue;
                processed = true;
                break;
            }
        }
        if (processed) {
            return sum;
        } else {
            return parseNumerals(text);
        }
    }

    public static Long parseNumerals(String text) {

        long sum = 0;
        // wrap hundert with space
        text = text.replaceAll(GermanNumerals.HUNDRED.getNumeral(),
                " " + GermanNumerals.HUNDRED.getNumeral() + " ");

        String[] words = text.split("\\s");
        for (String word : words) {
            Long numeralValue = GermanNumerals.getValue(word);
            if (numeralValue == null) {
                numeralValue = parseOnesBeforeTens(word);
                if (numeralValue == null) {
                    log.debug("Unknown token :" + word);
                    return null;
                }
            }
            if (numeralValue == GermanNumerals.HUNDRED.getValue()) {
                if (sum == GermanNumerals.ZERO.getValue()) {
                    sum = GermanNumerals.HUNDRED.getValue();
                } else {
                    sum *= GermanNumerals.HUNDRED.getValue();
                }
            } else {
                sum += numeralValue;
            }
        }
        return sum;
    }

    private static Long parseOnesBeforeTens(String word) {
        Long result = null;
        if (word.contains(UND_WORD)) {
            String[] words = word.split(UND_PATTERN);
            if (words.length == 2) {
                Long value1 = GermanNumerals.getValue(words[0]);
                Long value2 = GermanNumerals.getValue(words[1]);
                if (value1 != null && value2 != null) {
                    result = value1 + value2;
                }
            }
        }
        return result;
    }
}
