package gateprotecttask.server;

import java.util.ArrayList;
import java.util.List;

import gateprotecttask.client.ExpressionCalculatorService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class ExpressionCalculatorServiceImpl extends RemoteServiceServlet implements
        ExpressionCalculatorService {

    private static final String EXPRESSION_EVALUATION_ERROR = "Cannot evalueate expression.";

    @Override
    public List<String> calculateExpressions(String input) {
        String inputExpressions = escapeHtml(input);
        List<String> result = new ArrayList<String>();

        String[] expressions = inputExpressions.split("\n");
        for (String expression : expressions) {
            Integer value = ExpressionParser.evaluateExpression(expression);
            if (value != null) {
                result.add(String.valueOf(value));
            } else {
                result.add(EXPRESSION_EVALUATION_ERROR);
            }
        }

        return result;
    }

    /**
     * Escape an html string. Escaping data received from the client helps to
     * prevent cross-site script vulnerabilities.
     * 
     * @param html
     *            the html string to escape
     * @return the escaped string
     */
    private String escapeHtml(String html) {
        if (html == null) {
            return null;
        }
        return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
    }
}
