package gateprotecttask.server;

import java.util.regex.*;
import java.util.LinkedList;

/**
 * Splits string by default or given pattern.
 * 
 * @author Igor Poletaev
 * 
 */
public class Splitter {
    private static final Pattern DEFAULT_PATTERN = Pattern.compile("\\s+");

    private Pattern pattern;
    private boolean keepDelimiters;

    public Splitter(Pattern pattern, boolean keepDelimiters) {
        this.pattern = pattern;
        this.keepDelimiters = keepDelimiters;
    }

    public Splitter(String pattern, boolean keeDelimiters) {
        this(Pattern.compile(pattern == null ? "" : pattern), keeDelimiters);
    }

    public Splitter(Pattern pattern) {
        this(pattern, true);
    }

    public Splitter(String pattern) {
        this(pattern, true);
    }

    public Splitter(boolean keepDelimiters) {
        this(DEFAULT_PATTERN, keepDelimiters);
    }

    public Splitter() {
        this(DEFAULT_PATTERN);
    }

    public LinkedList<String> split(String text) {
        if (text == null) {
            text = "";
        }

        int lastMatch = 0;
        LinkedList<String> splitted = new LinkedList<String>();

        Matcher m = this.pattern.matcher(text);

        while (m.find()) {

            splitted.add(text.substring(lastMatch, m.start()));

            if (keepDelimiters) {
                splitted.add(m.group());
            }

            lastMatch = m.end();
        }

        splitted.add(text.substring(lastMatch));

        return splitted;
    }
}