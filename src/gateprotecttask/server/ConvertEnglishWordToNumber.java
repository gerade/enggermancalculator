package gateprotecttask.server;

import org.apache.log4j.Logger;

import gateprotecttask.server.model.EnglishNumerals;
import gateprotecttask.server.model.EnglishPowerNumerals;

/**
 * Converts English word numerals to long.
 * 
 * @author Igor Poletaev
 * 
 */
public class ConvertEnglishWordToNumber {
    private static Logger log = Logger.getLogger(ConvertEnglishWordToNumber.class);

    public static Long parse(String text) {

        // "-" is useless and can be removed
        text = text.toLowerCase().replaceAll("[\\-,]", " ").replaceAll(" and ", " ");
        long sum = 0L;
        boolean processed = false;
        for (int i = 0; i < EnglishPowerNumerals.values().length; i++) {
            int index = text.indexOf(EnglishPowerNumerals.values()[i].getNumeral());
            if (index >= 0) {

                String before = text.substring(0, index).trim();

                String after = text.substring(
                        index + EnglishPowerNumerals.values()[i].getNumeral().length()).trim();

                if (before.isEmpty()) {
                    before = EnglishNumerals.ONE.getNumeral();
                }
                if (after.isEmpty()) {
                    after = EnglishNumerals.ZERO.getNumeral();
                }
                Long beforeValue = parseNumerals(before);

                Long afterValue = parse(after);
                if (beforeValue == null || afterValue == null) {
                    return null;
                }
                sum = beforeValue * EnglishPowerNumerals.values()[i].getValue() + afterValue;

                processed = true;
                break;
            }
        }
        if (processed) {
            return sum;
        } else {
            return parseNumerals(text);
        }
    }

    public static Long parseNumerals(String text) throws NumberFormatException {

        long sum = 0;
        String[] words = text.split("\\s");
        for (String word : words) {
            Long numeralValue = EnglishNumerals.getValue(word);
            if (numeralValue == null) {
                log.debug("Unknown token : " + word);
                return null;
            }
            if (numeralValue == EnglishNumerals.HUNDRED.getValue()) {
                if (sum == EnglishNumerals.ZERO.getValue()) {
                    sum = EnglishNumerals.HUNDRED.getValue();
                } else {
                    sum *= EnglishNumerals.HUNDRED.getValue();
                }
            } else {
                sum += numeralValue;
            }
        }
        return sum;
    }
}
