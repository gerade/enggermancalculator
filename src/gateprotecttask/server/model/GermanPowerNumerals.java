package gateprotecttask.server.model;

/**
 * English numerals for thousand and million. NOTE: sequence is important. Lower
 * powers should follow higher ones.
 * 
 * @author Igor Poletaev
 * 
 */
public enum GermanPowerNumerals {
    MILLION("millionen", 1000000), MILLION1("million", 1000000), THOUSAND("tausend", 1000);

    private final String numeral;
    private final long value;

    GermanPowerNumerals(String numeral, long value) {
        this.numeral = numeral;
        this.value = value;

    }

    public static boolean containsNumeralIgnoreCase(String str) {
        for (GermanPowerNumerals element : values()) {
            if (element.numeral.toString().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    public String getNumeral() {
        return numeral;
    }

    public long getValue() {
        return value;
    }
}
