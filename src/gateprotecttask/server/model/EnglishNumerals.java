package gateprotecttask.server.model;

/**
 * English numerals from 1 to 100.
 * 
 * @author Igor Poletaev
 * 
 */
public enum EnglishNumerals {
    ZERO("zero", 0), ONE("one", 1), TWO("two", 2), THREE("three", 3), FOUR("four", 4), FIVE("five",
            5), SIX("six", 6), SEVEN("seven", 7), EIGHT("eight", 8), NINE("nine", 9), TEN("ten", 10), ELEVEN(
            "eleven", 11), TWELVE("twelve", 12), THIRTEEN("thirteen", 13), FOURTEEN("fourteen", 14), FIFTEEN(
            "fifteen", 15), SIXTEEN("sixteen", 16), SEVENTEEN("seventeen", 17), EIGHTEEN(
            "eighteen", 18), NINETEEN("nineteen", 19), TWENTY("twenty", 20), THIRTY("thirty", 30), FORTY(
            "forty", 40), FIFTY("fifty", 50), SIXTY("sixty", 60), SEVENTY("seventy", 70), EIGHTY(
            "eighty", 80), NINETY("ninety", 90), HUNDRED("hundred", 100);

    private final String numeral;
    private final long value;

    EnglishNumerals(String numeral, long value) {
        this.numeral = numeral;
        this.value = value;

    }

    public static boolean containsNumeralIgnoreCase(String str) {
        for (EnglishNumerals element : values()) {
            if (element.numeral.toString().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    public static Long getValue(String numeral) {
        for (EnglishNumerals element : values()) {
            if (element.numeral.toString().equalsIgnoreCase(numeral)) {
                return element.value;
            }
        }
        return null;
    }

    public String getNumeral() {
        return numeral;
    }

    public long getValue() {
        return value;
    }
}
