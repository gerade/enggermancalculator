package gateprotecttask.server.model;

/**
 * Enumeration which holds division numerals.
 * 
 * @author Igor Poletaev
 * 
 */
public enum Division {
    DIVISION_SYMBOL("/"), DIVISION_ENGLISH1("divide by"), DIVISION_ENGLISH2("divided by"), DIVISION_GERMAN1(
            "teilen"), DIVISION_GERMAN2("durch"), DIVISION_GERMAN3("geteilt durch");

    private final String value;

    Division(String value) {
        this.value = value;

    }

    @Override
    public String toString() {
        return value;
    }

    public static boolean containsIgnoreCase(String str) {
        for (Division element : values()) {
            if (element.toString().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }
}
