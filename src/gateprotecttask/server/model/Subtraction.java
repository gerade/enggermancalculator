package gateprotecttask.server.model;

/**
 * Enumeration which holds subtraction numerals.
 * 
 * @author Igor Poletaev
 * 
 */
public enum Subtraction {
    MINUS_SYMBOL("-"), MINUS_ENGLISH1("subtract"), MINUS_ENGLISH2("minus"), MINUS_GERMAN(
            "Minus");

    private final String value;

    Subtraction(String value) {
        this.value = value;

    }

    @Override
    public String toString() {
        return value;
    }

    public static boolean containsIgnoreCase(String str) {
        for (Subtraction element : values()) {
            if (element.toString().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }
}
