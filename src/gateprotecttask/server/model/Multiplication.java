package gateprotecttask.server.model;

/**
 * Enumeration which holds multiplication numerals.
 * 
 * @author Igor Poletaev
 * 
 */
public enum Multiplication {
    MULTIPLY_SYMBOL("*"), MULTIPLY_ENGLISH1("multiply with"), MULTIPLY_ENGLISH2("times"), MULTIPLY_GERMAN(
            "mal");

    private final String value;

    Multiplication(String value) {
        this.value = value;

    }

    @Override
    public String toString() {
        return value;
    }

    public static boolean containsIgnoreCase(String str) {
        for (Multiplication element : values()) {
            if (element.toString().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }
}
