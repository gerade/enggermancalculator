package gateprotecttask.server.model;
/**
 * Enumeration which holds round brackets numerals.
 * 
 * @author Igor Poletaev
 * 
 */
public enum Brackets {
	OPEN("("), CLOSE(")");

	private final String value;

	Brackets(String value) {
		this.value = value;

	}

	@Override
	public String toString() {
		return value;
	}

}
