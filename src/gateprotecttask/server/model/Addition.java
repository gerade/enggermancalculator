package gateprotecttask.server.model;

/**
 * Enumeration which holds addition numerals.
 * 
 * @author Igor Poletaev
 * 
 */
public enum Addition {
    PLUS_SYMBOL("+"), PLUS_ENGLISH1("add"), PLUS_ENGLISH2("plus"), PLUS_GERMAN("plus");

    private final String value;

    Addition(String value) {
        this.value = value;

    }

    @Override
    public String toString() {
        return value;
    }

    public static boolean containsIgnoreCase(String str) {
        for (Addition element : values()) {
            if (element.toString().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }
}
