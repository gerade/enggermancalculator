package gateprotecttask.server.model;

/**
 * English numerals from 1 to 100.
 * 
 * @author Igor Poletaev
 * 
 */
public enum GermanNumerals {
    ZERO("null", 0), ONE("eins", 1), ONE1("ein", 1), TWO("zwei", 2), THREE("drei", 3), FOUR("vier",
            4), FIVE("fünf", 5), SIX("sechs", 6), SEVEN("sieben", 7), EIGHT("acht", 8), NINE(
            "neun", 9), TEN("zehn", 10), ELEVEN("elf", 11), TWELVE("zwölf", 12), THIRTEEN(
            "dreizehn", 13), FOURTEEN("vierzehn", 14), FIFTEEN("fünfzehn", 15), SIXTEEN("sechzehn",
            16), SEVENTEEN("siebzehn", 17), EIGHTEEN("achtzehn", 18), NINETEEN("neunzehn", 19), TWENTY(
            "zwanzig", 20), THIRTY("dreißig", 30), FORTY("vierzig", 40), FIFTY("fünfzig", 50), SIXTY(
            "sechzig", 60), SEVENTY("siebzig", 70), EIGHTY("achtzig", 80), NINETY("neunzig", 90), HUNDRED(
            "hundert", 100);

    private final String numeral;
    private final long value;

    GermanNumerals(String numeral, long value) {
        this.numeral = numeral;
        this.value = value;

    }

    public static boolean containsNumeralIgnoreCase(String str) {
        for (GermanNumerals element : values()) {
            if (element.numeral.toString().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    public static Long getValue(String numeral) {
        for (GermanNumerals element : values()) {
            if (element.numeral.toString().equalsIgnoreCase(numeral)) {
                return element.value;
            }
        }
        return null;
    }

    public String getNumeral() {
        return numeral;
    }

    public long getValue() {
        return value;
    }
}
