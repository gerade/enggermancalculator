package gateprotecttask.client;

import gateprotecttask.client.event.CalculationResultsEvent;
import gateprotecttask.client.event.CalculationResultsEventHandler;
import gateprotecttask.client.presenter.CalculatorPresenter;
import gateprotecttask.client.presenter.Presenter;
import gateprotecttask.client.view.CalculatorView;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;

/**
 * Provides initial support of application scope events handling using EventBus
 * and History API. Both are currently not used.
 * 
 * @author Igor Poletaev
 * 
 */
public class AppController implements Presenter, ValueChangeHandler<String> {
    private final HandlerManager eventBus;
    private final ExpressionCalculatorServiceAsync rpcService;
    private HasWidgets container;

    public AppController(ExpressionCalculatorServiceAsync rpcService, HandlerManager eventBus) {
        this.eventBus = eventBus;
        this.rpcService = rpcService;
        bind();
    }

    private void bind() {
        History.addValueChangeHandler(this);

        eventBus.addHandler(CalculationResultsEvent.TYPE, new CalculationResultsEventHandler() {

            @Override
            public void onCalculationResults(CalculationResultsEvent event) {
                // results can be got from event.getExpressionEvaluationResults
                // New item can be added to History
            }
        });

    }

    /**
     * This method is called when everything is wired up.
     * 
     * @param container
     */
    public void go(final HasWidgets container) {
        this.container = container;

        if ("".equals(History.getToken())) {
            // just initial state
            // Currently History is not used.
            History.newItem("calcres");
        } else {
            History.fireCurrentHistoryState();
        }
    }

    /**
     * History event handling.
     * 
     * @param event
     *            history event
     */
    public void onValueChange(ValueChangeEvent<String> event) {
        String token = event.getValue();

        if (token != null) {
            Presenter presenter = null;

            if (token.equals("calcres")) {
                presenter = new CalculatorPresenter(rpcService, eventBus, new CalculatorView());
            }
            // add new screens here
            if (presenter != null) {
                presenter.go(container);
            }
        }
    }
}
