package gateprotecttask.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Handler interface for <code>CalculationResultsEvent</code>.
 * 
 * @author Igor Poletaev
 * 
 */
public interface CalculationResultsEventHandler extends EventHandler {
    void onCalculationResults(CalculationResultsEvent event);
}
