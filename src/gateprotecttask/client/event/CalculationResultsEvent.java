package gateprotecttask.client.event;

import java.util.List;

import com.google.gwt.event.shared.GwtEvent;

/**
 * This event is fired when evaluation results are reveiced. Currently it has no
 * practical purpose.
 * 
 * @author Igor Poletaev
 * 
 */
public class CalculationResultsEvent extends GwtEvent<CalculationResultsEventHandler> {
    public static Type<CalculationResultsEventHandler> TYPE = new Type<CalculationResultsEventHandler>();
    private final List<String> expressionEvaluationResults;

    public CalculationResultsEvent(List<String> results) {
        this.expressionEvaluationResults = results;
    }

    @Override
    public Type<CalculationResultsEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CalculationResultsEventHandler handler) {
        handler.onCalculationResults(this);
    }

    public List<String> getExpressionEvaluationResults() {
        return expressionEvaluationResults;
    }
}