package gateprotecttask.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 * 
 * @author Igor Poletaev
 */
@RemoteServiceRelativePath("expressionCalculator")
public interface ExpressionCalculatorService extends RemoteService {

    List<String> calculateExpressions(String expressionInput);
}
