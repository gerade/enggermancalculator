package gateprotecttask.client.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasDirectionalHtml;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import gateprotecttask.client.presenter.CalculatorPresenter;

/**
 * This view contains all of the UI components that make up our application.
 * 
 * @author Igor Poletaev
 * 
 */
public class CalculatorView extends Composite implements CalculatorPresenter.Display {
    /**
     * This tip text will be presented to the user.
     */
    private static final String TIP = "<b>Tip:</b> type expressions using numbers and English and German numerals. <br>"
            + "<b>Important:</b> In case of complex expressions it is obligatory to specify operation priority using brackts.<br>"
            + "<b>Examples:</b><br>"
            + "(((hundred and one subtract six )*(minus one minus seven))/(two minus eleven)) <br>"
            + "(((5 add 1) subtract 3 )divide by 3) multiply with 7 <br>"
            + "(((100 - 5 )*(-1-7))/(2-11)) <br>" +
            "neun Millionen neunhundertneunundneunzigtausendneunhundertneunundneunzig minus 1 <br>" +
            "(Minus Millionen neunhundertneunundneunzigtausendneunhundertneunundneunzig + 1.999.999)";
    /**
     * The message displayed to the user when the server cannot be reached or
     * returns an error.
     */
    public static final String SERVER_ERROR = "An error occurred while "
            + "attempting to contact the server. Please check your network "
            + "connection and try again.";
    public static final String HTML_LINE_BREAK = "<br>";
    public static final String EMPTY_INPUT_MESSAGE = "Nothing to evaluate. Input is empty.";
    private static final String TITLE_TEXT = "Calculator with a \"German and English Numerals\" function";
    private final Button calculateButton = new Button("Evaluate&nbsp;Expressions");
    private TextArea input = new TextArea();
    private HTML output = new HTML();

    public CalculatorView() {
        DecoratorPanel decoratorPanel = new DecoratorPanel();
        initWidget(decoratorPanel);

        input.setStylePrimaryName("input-textarea");
        output.setStylePrimaryName("output-html");

        HTML title = new HTML(TITLE_TEXT);
        HorizontalPanel contentPanel = new HorizontalPanel();

        contentPanel.setSpacing(10);
        contentPanel.add(input);
        contentPanel.add(calculateButton);
        contentPanel.setCellVerticalAlignment(calculateButton, HasVerticalAlignment.ALIGN_MIDDLE);
        contentPanel.add(output);

        HTML tip = new HTML(TIP);

        VerticalPanel vPanel = new VerticalPanel();

        vPanel.add(title);
        title.setStylePrimaryName("title");
        vPanel.add(contentPanel);
        vPanel.add(tip);
        vPanel.setCellHorizontalAlignment(title, HasHorizontalAlignment.ALIGN_CENTER);
        decoratorPanel.add(vPanel);
    }

    public Widget asWidget() {
        return this;
    }

    @Override
    public HasClickHandlers getCalculateButton() {
        return calculateButton;
    }

    @Override
    public HasValue<String> getExpressionInput() {
        return input;
    }

    @Override
    public HasDirectionalHtml getExpressionOutput() {
        return output;
    }
}
