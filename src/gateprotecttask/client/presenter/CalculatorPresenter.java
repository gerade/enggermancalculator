package gateprotecttask.client.presenter;

import java.util.List;

import gateprotecttask.client.ExpressionCalculatorServiceAsync;
import gateprotecttask.client.event.CalculationResultsEvent;
import gateprotecttask.client.view.CalculatorView;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasDirectionalHtml;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.Window;

/**
 * Presenter for <code>CalculatorView</code>
 * 
 * @author Igor Poletaev
 * 
 */
public class CalculatorPresenter implements Presenter {

    public interface Display {
        HasClickHandlers getCalculateButton();

        HasValue<String> getExpressionInput();

        HasDirectionalHtml getExpressionOutput();

        Widget asWidget();
    }

    private final ExpressionCalculatorServiceAsync rpcService;
    private final HandlerManager eventBus;
    private final Display display;

    public CalculatorPresenter(ExpressionCalculatorServiceAsync rpcService,
            HandlerManager eventBus, Display display) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.display = display;
        bind();
    }

    public void bind() {
        this.display.getCalculateButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                // check that input is not empty
                if (!display.getExpressionInput().getValue().replaceAll("\\s", "").isEmpty()) {
                    calculateExpressions();
                } else {
                    display.getExpressionOutput().setHTML(
                            CalculatorView.EMPTY_INPUT_MESSAGE);
                }
            }
        });
    }

    public void go(final HasWidgets container) {
        container.clear();
        container.add(display.asWidget());
    }

    private void calculateExpressions() {

        rpcService.calculateExpressions(display.getExpressionInput().getValue(),
                new AsyncCallback<List<String>>() {
                    public void onSuccess(List<String> result) {
                        StringBuilder content = new StringBuilder();

                        for (String line : result) {
                            content.append(line);
                            content.append(CalculatorView.HTML_LINE_BREAK);
                        }
                        display.getExpressionOutput().setHTML(content.toString());

                        // fire event and pass results to the AppController.
                        // can be used to process results on application level
                        eventBus.fireEvent(new CalculationResultsEvent(result));
                    }

                    public void onFailure(Throwable caught) {
                        Window.alert(CalculatorView.SERVER_ERROR);
                    }
                });
    }

}
