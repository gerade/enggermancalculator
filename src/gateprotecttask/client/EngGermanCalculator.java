package gateprotecttask.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>. Is called by GWT
 * bootstrap process.
 * 
 * @author Igor Poletaev
 * 
 */
public class EngGermanCalculator implements EntryPoint {

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {
        ExpressionCalculatorServiceAsync rpcService = GWT.create(ExpressionCalculatorService.class);
        HandlerManager eventBus = new HandlerManager(null);
        AppController appViewer = new AppController(rpcService, eventBus);
        appViewer.go(RootPanel.get("main"));

    }
}
