package gateprotecttask.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>ExpressionCalculatorService</code>.
 * 
 * @author Igor Poletaev
 */
public interface ExpressionCalculatorServiceAsync {

    void calculateExpressions(String expressionInput, AsyncCallback<List<String>> asyncCallback);
}
